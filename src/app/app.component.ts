import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  listPosts = [
  {
    postTitle: 'post_1',
    postContent: 'Houlalala',
    postLoveIts: 10,
    postCreatedAt: new Date()
  },
  {
    postTitle: 'post_2',
    postContent: 'Houlalala2',
    postLoveIts: 1,
    postCreatedAt: new Date()
  }
  ];

}
